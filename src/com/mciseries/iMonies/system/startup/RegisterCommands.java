package com.mciseries.iMonies.system.startup;

import com.mciseries.iMonies.iMonies;
import com.mciseries.iMonies.system.CommandExecutor;

/**
 * @author rtainc
 *
 */
public class RegisterCommands {
	/**
	 * @param p
	 */
	public RegisterCommands(iMonies p) {
		p.getCommand("money").setExecutor(new CommandExecutor());
	}
}
