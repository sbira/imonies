package com.mciseries.iMonies.system.startup;

import org.bukkit.plugin.ServicePriority;

import com.mciseries.iMonies.iMonies;
import com.mciseries.iMonies.api.Account;

/**
 * @author rtainc
 *
 */
public class VaultHooking {
	iMonies plugin;
	/**
	 * @param plugin
	 */
	public VaultHooking(iMonies plugin) {
		this.plugin = plugin;
	}
	/**
	 * @return Whether Vault is installed and hooked or not
	 */
	public boolean hook() {
		if(plugin.getServer().getPluginManager().getPlugin("Vault") == null) {
			plugin.getLogger().warning("You need Vault (http://dev.bukkit.org/server-mods/vault) for iMonies to function with most plugins.");
			return false;
		}
		if(plugin.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class) != null) {
			plugin.getServer().getServicesManager().unregister(plugin.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class).getProvider());
		}
		plugin.getServer().getServicesManager().register(net.milkbowl.vault.economy.Economy.class, new Account(plugin), plugin, ServicePriority.Highest);
		return true;
	}
	public void unhook() {
		plugin.getServer().getServicesManager().unregisterAll(plugin.getServer().getPluginManager().getPlugin("iMonies"));
	}
}
