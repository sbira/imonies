package com.mciseries.iMonies.system.startup;

import com.mciseries.iMonies.iMonies;
import com.mciseries.iMonies.system.listeners.*;
import com.mciseries.iMonies.system.Interest;

/**
 * @author rtainc
 *
 */
public class RegisterEvents {
	/**
	 * @param p
	 */
	public RegisterEvents(iMonies p) {
		p.getServer().getPluginManager().registerEvents(new Join(), p);
		p.getServer().getPluginManager().registerEvents(new Death(), p);
		
		// gonna disable signbanks for now; getting InterruptedException on my chat event. will come back in 3.3?
		// p.getServer().getPluginManager().registerEvents(new Sign(), p);
		// p.getServer().getPluginManager().registerEvents(new Chat(), p);
		new Interest().startTimer();
	}
}
