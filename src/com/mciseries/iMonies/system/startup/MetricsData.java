package com.mciseries.iMonies.system.startup;

import java.io.IOException;

import com.mciseries.iMonies.iMonies;
import com.mciseries.iMonies.system.Utils;
import com.mciseries.iMonies.system.extras.Metrics;
import com.mciseries.iMonies.system.extras.Metrics.Graph;
import com.mciseries.iMonies.system.files.Config;

/**
 * @author rtainc
 *
 */
public class MetricsData {
	Config c = new Config();
	/**
	 * @param p
	 */
	public MetricsData(iMonies p) {
		try {
			Metrics m = new Metrics(p);
			Graph g = m.createGraph("Loans");
			g.addPlotter(new Metrics.Plotter("Enabled") {
				@Override
				public int getValue() {
					if(Utils.useLoans) {
						return 1;
					}
					else {
						return 0;
					}
				}
			});
			g.addPlotter(new Metrics.Plotter("Disabled") {
				@Override
				public int getValue() {
					if(Utils.useLoans) {
						return 0;
					}
					else {
						return 1;
					}
				}
			});
			Graph g2 = m.createGraph("Interest");
			g2.addPlotter(new Metrics.Plotter("Enabled") {
				@Override
				public int getValue() {
					if(Utils.useInterest) {
						return 1;
					}
					else {
						return 0;
					}
				}
			});
			g2.addPlotter(new Metrics.Plotter("Disabled") {
				@Override
				public int getValue() {
					if(Utils.useInterest) {
						return 0;
					}
					else {
						return 1;
					}
				}
			});
			Graph g3 = m.createGraph("MySQL");
			g3.addPlotter(new Metrics.Plotter("Enabled") {
				@Override
				public int getValue() {
					if(Utils.useMySQL) {
						return 1;
					}
					else {
						return 0;
					}
				}
			});
			g3.addPlotter(new Metrics.Plotter("Disabled") {
				@Override
				public int getValue() {
					if(Utils.useMySQL) {
						return 0;
					}
					else {
						return 1;
					}
				}
			});
			Graph g4 = m.createGraph("Banks");
			g4.addPlotter(new Metrics.Plotter("Enabled") {
				@Override
				public int getValue() {
					if(Utils.useBanks) {
						return 1;
					}
					else {
						return 0;
					}
				}
			});
			g4.addPlotter(new Metrics.Plotter("Disabled") {
				@Override
				public int getValue() {
					if(Utils.useBanks) {
						return 0;
					}
					else {
						return 1;
					}
				}
			});
			Graph g5 = m.createGraph("Banks (Signs)");
			g5.addPlotter(new Metrics.Plotter("Enabled") {
				@Override
				public int getValue() {
					if(Utils.useBankSigns)
						return 1;
					else
						return 0;
				}
			});
			g5.addPlotter(new Metrics.Plotter("Disabled") {
				@Override
				public int getValue() {
					if(Utils.useBankSigns)
						return 0;
					else
						return 1;
				}
			});
			Graph g6 = m.createGraph("Banks (Commands)");
			g6.addPlotter(new Metrics.Plotter("Enabled") {
				@Override
				public int getValue() {
					if(Utils.useBankCommands)
						return 1;
					else
						return 0;
				}
			});
			g6.addPlotter(new Metrics.Plotter("Disabled") {
				@Override
				public int getValue() {
					if(Utils.useBankCommands)
						return 0;
					else
						return 1;
				}
			});
			m.start();
		} catch (IOException e) {
			
		}
	}
}
