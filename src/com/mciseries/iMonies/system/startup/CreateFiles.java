package com.mciseries.iMonies.system.startup;

import java.io.IOException;

import com.mciseries.iMonies.system.files.*;
import com.mciseries.iMonies.system.storage.Storage;

/**
 * @author rtainc
 *
 */
public class CreateFiles {
	public CreateFiles() {
		try {
			new Config().saveDefaults();
		}
		catch(IOException e) {
			
		}
		try {
			new Messages().saveDefaults();
		}
		catch(IOException e) {
			
		}
		Storage.initialize();
	}
}
