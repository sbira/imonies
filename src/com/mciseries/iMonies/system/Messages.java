package com.mciseries.iMonies.system;

/**
 * @author rtainc
 *
 */
public class Messages {
	/**
	 * @param message
	 * @param name
	 * @param amount
	 * @param balance
	 * @return The formatted message
	 */
	public String getMessage(String message, String name, String amount, String balance) {
		return "[\u00A76iMonies\u00A7r] " + new com.mciseries.iMonies.system.files.Messages().getMessage(message).replace("<name>", name).replace("<amount>", amount).replace("<balance>", balance).replace("&", "\u00A7").replace("\u00A7 ", "& ");
	}
	/**
	 * @param message
	 * @return The formatted error message
	 */
	public String getErrorMessage(String message) {
		return "[\u00A76iMonies\u00A7r] " + new com.mciseries.iMonies.system.files.Messages().getErrorMessage(message).replace("&", "\u00A7").replace("\u00A7 ", "& ");
	}
	/**
	 * @param message
	 * @return The formatted help message
	 */
	public String getHelpMessage(String message) {
		return new com.mciseries.iMonies.system.files.Messages().getHelpMessage(message).replace("&", "\u00A7").replace("\u00A7 ", "& ");
	}
}
