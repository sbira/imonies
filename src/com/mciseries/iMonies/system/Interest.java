package com.mciseries.iMonies.system;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.mciseries.iMonies.api.Account;
import com.mciseries.iMonies.api.Bank;

/**
 * @author rtainc
 *
 */
public class Interest {
	public Interest() {}
	Account api = new Account(Bukkit.getPluginManager().getPlugin("iMonies"));
	Bank bapi = new Bank();
	Messages messages = new Messages();
	ScheduledFuture<?> interest = null;
	public void startTimer() {
		if(Utils.useInterest) {
			ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
			this.interest = service.scheduleAtFixedRate(
					new Runnable() {
						public void run() {
							for(Player p : Bukkit.getOnlinePlayers()) {
								double b;
								if(!Utils.useInterestFromBank)
									b = api.getBalance(p.getName());
								else
									b = bapi.getBalance(p.getName());
								double a;
								if(Utils.interestCutoff <= api.getBalance(p.getName()) + bapi.getBalance(p.getName()))
									return;
								if(Utils.percentageInterest) {
									StringBuilder ab = new StringBuilder("");
									ab.append(".");
									ab.append("" + Utils.interestAmount);
									double ad = Double.parseDouble(ab.toString());
									a = b * ad;
								}
								else {
									a = Utils.interestAmount;
								}
								p.sendMessage(messages.getMessage("Interest", "", "" + Utils.interestInterval, "" + a));
								api.depositPlayer(p.getName(), a);
							}
						}
					}
				, (long) Utils.interestInterval, (long) Utils.interestInterval, TimeUnit.SECONDS);
		}
		else {
			
		}
	}
}
