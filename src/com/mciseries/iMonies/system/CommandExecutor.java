package com.mciseries.iMonies.system;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.iMonies;
import com.mciseries.iMonies.handlers.*;
import com.mciseries.iMonies.handlers.bank.*;

/**
 * @author rtainc
 *
 */
@SuppressWarnings("unused")
public class CommandExecutor implements org.bukkit.command.CommandExecutor {
	private Messages msg = new Messages();
	private FileOutputStream fos;
	/* (non-Javadoc)
	 * @see org.bukkit.command.CommandExecutor#onCommand(org.bukkit.command.CommandSender, org.bukkit.command.Command, java.lang.String, java.lang.String[])
	 */
	@Override
	public boolean onCommand(CommandSender s, Command c, String st, String[] a) {
		if(a.length < 1) {
			new Money(s, c, a, false);
		}
		else {
			if(a[0].equalsIgnoreCase("set"))
				new Set(s, c, a);
			else if(a[0].equalsIgnoreCase("convert"))
				(new Thread(new Convert(s, c, a))).start();
			else if(a[0].equalsIgnoreCase("transfer"))
				new Transfer(s, c, a);
			else if(a[0].equalsIgnoreCase("create"))
				new Create(s, c, a);
			else if(a[0].equalsIgnoreCase("remove"))
				new Remove(s, c, a);
			else if(a[0].equalsIgnoreCase("setowes"))
				new SetOwes(s, c, a);
			else if(a[0].equalsIgnoreCase("loan"))
				new Loan(s, c, a);
			else if(a[0].equalsIgnoreCase("payoffloan"))
				new PayOffLoan(s, c, a);
			else if(a[0].equalsIgnoreCase("pay"))
				new Pay(s, c, a);
			else if(a[0].equalsIgnoreCase("empty"))
				new Empty(s, c, a);
			else if(a[0].equalsIgnoreCase("help"))
				new Help(s, c, a);
			else if(a[0].equalsIgnoreCase("give"))
				new Give(s, c, a);
			else if(a[0].equalsIgnoreCase("take"))
				new Take(s, c, a);
			else if(a[0].equalsIgnoreCase("purge"))
				new Purge(s, c, a);
			else if(a[0].equalsIgnoreCase("payall"))
				new PayAll(s, c, a);
			else if(a[0].equalsIgnoreCase("payalloffline"))
				new PayAllOffline(s, c, a);
			else if(a[0].equalsIgnoreCase("top"))
				new Top(s, c, a);
			else if(a[0].equalsIgnoreCase("reload"))
				new Reload(s, c, a);
			else if(a[0].equalsIgnoreCase("about"))
				new About(s, c, a);
			else if(a[0].equalsIgnoreCase("update"))
				new com.mciseries.iMonies.handlers.Update(s, c, a);
			else if(a[0].equalsIgnoreCase("bank")) {
				if(a.length < 2)
					s.sendMessage(msg.getErrorMessage("NotEnoughArgs"));
				else
					if(a[1].equalsIgnoreCase("deposit"))
						new Deposit(s, c, a);
					else if(a[1].equalsIgnoreCase("withdraw"))
						new Withdraw(s, c, a);
			}
			else
				new Money(s, c, a, true);
		}
		return true;
	}
	
}
