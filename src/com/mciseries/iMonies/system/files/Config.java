package com.mciseries.iMonies.system.files;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Config {
	File configFile = new File("plugins" + File.separator + "iMonies" + File.separator + "Config.yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);
	/**
	 * @throws IOException
	 */
	public void saveDefaults() throws IOException {
		String[] allOptions = {"DefaultBalance;100.0", "SingularName;Dollar", "PluralName;Dollars", "MoneyLimit;100000.0", "Debug;false", "ThousandsSeparator;,", "CentsSeparator;.", "TopAccountsPerPage;5"};
		String[] allMySQLOptions = {"Use;false", "IP;localhost", "DbName;imonies", "Port;3306", "Username;root", "Password; ", "Table;accounts"};
		String[] allInterestOptions = {"Use;false", "Interval;3600.0", "Percentage;false", "Amount;5.0", "UseMoneyFromBank;false", "Cutoff;10000.0"};
		String[] allLoanOptions = {"Use;false", "MaxOwed;25.0", "PercentageExtraPerPayment;10.0"};
		String[] allBankOptions = {"Use;false", "LoseMoneyOnDeath;false", "UseSigns;true", "UseCommands;true", "SignCreationCost;10.0"};
		for(String all : allOptions) {
			String[] parts = all.split("\\;");
			String option = parts[0];
			String defaultValue = parts[1];
			if(config.getString(option) == null) {
				if(defaultValue.contains(".0")) {
					config.set(option, Double.parseDouble(defaultValue));
				}
				else if(defaultValue.equalsIgnoreCase("1") || defaultValue.equalsIgnoreCase("2") || defaultValue.equalsIgnoreCase("3") || defaultValue.equalsIgnoreCase("4") || defaultValue.equalsIgnoreCase("5"))
					config.set(option, Integer.parseInt(defaultValue));
				else if(defaultValue.equalsIgnoreCase("true") || defaultValue.equalsIgnoreCase("false")) {
					config.set(option, Boolean.parseBoolean(defaultValue));
				}
				else {
					config.set(option, defaultValue);
				}
			}
			config.save(configFile);
		}
		for(String all : allMySQLOptions) {
			String[] parts = all.split("\\;");
			String option = parts[0];
			String defaultValue = parts[1];
			if(config.getString("MySQL." + option) == null) {
				if(defaultValue.equalsIgnoreCase("true") || defaultValue.equalsIgnoreCase("false")) {
					config.set("MySQL." + option, Boolean.parseBoolean(defaultValue));
				}
				else {
					config.set("MySQL." + option, defaultValue);
				}
			}
			config.save(configFile);
		}
		for(String all : allInterestOptions) {
			String[] parts = all.split("\\;");
			String option = parts[0];
			String defaultValue = parts[1];
			if(config.getString("Interest." + option) == null) {
				if(defaultValue.contains(".0")) {
					config.set("Interest." + option, Double.parseDouble(defaultValue));
				}
				else if(defaultValue.equalsIgnoreCase("true") || defaultValue.equalsIgnoreCase("false")) {
					config.set("Interest." + option, Boolean.parseBoolean(defaultValue));
				}
				else {
					config.set("Interest." + option, defaultValue);
				}
			}
			config.save(configFile);
		}
		for(String all : allLoanOptions) {
			String[] parts = all.split("\\;");
			String option = parts[0];
			String defaultValue = parts[1];
			if(config.getString("Loans." + option) == null) {
				if(defaultValue.contains(".0")) {
					config.set("Loans." + option, Double.parseDouble(defaultValue));
				}
				else if(defaultValue.equalsIgnoreCase("true") || defaultValue.equalsIgnoreCase("false")) {
					config.set("Loans." + option, Boolean.parseBoolean(defaultValue));
				}
				else {
					config.set("Loans." + option, defaultValue);
				}
			}
			config.save(configFile);
		}
		for(String all : allBankOptions) {
			String[] parts = all.split("\\;");
			String option = parts[0];
			String defaultValue = parts[1];
			if(config.getString("Banks." + option) == null) {
				if(defaultValue.contains(".0")) {
					config.set("Banks." + option, Double.parseDouble(defaultValue));
				}
				else if(defaultValue.equalsIgnoreCase("true") || defaultValue.equalsIgnoreCase("false")) {
					config.set("Banks." + option, Boolean.parseBoolean(defaultValue));
				}
				else {
					config.set("Banks." + option, defaultValue);
				}
			}
			config.save(configFile);
		}
		// set vars
		Utils.defaultBalance = config.getDouble("DefaultBalance");
		Utils.debug = config.getBoolean("Debug");
		Utils.singularName = config.getString("SingularName");
		Utils.pluralName = config.getString("PluralName");
		Utils.moneyLimit = config.getDouble("MoneyLimit");
		Utils.useMySQL = config.getBoolean("MySQL.Use");
		Utils.mySQLDatabase = config.getString("MySQL.DbName");
		Utils.mySQLIP = config.getString("MySQL.IP");
		Utils.mySQLPassword = config.getString("MySQL.Password");
		Utils.mySQLUsername = config.getString("MySQL.Username");
		Utils.mySQLTable = config.getString("MySQL.Table");
		Utils.mySQLPort = config.getString("MySQL.Port");
		Utils.useLoans = config.getBoolean("Loans.Use");
		Utils.useBanks = config.getBoolean("Banks.Use");
		Utils.useBankCommands = config.getBoolean("Banks.UseCommands");
		Utils.useBankSigns = config.getBoolean("Banks.UseSigns");
		Utils.maxOwed = config.getDouble("Loans.MaxOwed");
		Utils.topAccountsPerPage = config.getInt("TopAccountsPerPage");
		Utils.interestAmount = config.getDouble("Interest.Amount");
		Utils.interestCutoff = config.getDouble("Interest.Cutoff");
		Utils.interestInterval = config.getDouble("Interest.Interval");
		Utils.useInterest = config.getBoolean("Interest.Use");
		Utils.percentageInterest = config.getBoolean("Interest.Percentage");
		Utils.useInterestFromBank = config.getBoolean("Interest.UseMoneyFromBank");
		Utils.loseMoneyOnDeath = config.getBoolean("Banks.LoseMoneyOnDeath");
		Utils.thousandsSeparator = config.getString("ThousandsSeparator");
		Utils.centsSeparator = config.getString("CentsSeparator");
		Utils.signBankCreationCost = config.getDouble("Banks.SignCreationCost");
	}
}