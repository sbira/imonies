package com.mciseries.iMonies.system.files;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * @author rtainc
 *
 */
public class Messages {
	File configFile = new File("plugins" + File.separator + "iMonies" + File.separator + "Messages.yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);
	/**
	 * @throws IOException
	 */
	public void saveDefaults() throws IOException {
		String[] allMessages = {"Prefix;[&6iMonies&r]", "MoneyOthers;&7<name>'s balance: &6$<balance>", "Money;&7Balance: &6$<balance>", "OwesMoney;&cYou owe &6$<balance>&c! Pay off your loans!", "Set;&aChanged balance to &6$<balance>", "SetOwes;&aChanged amount owed to &6$<balance>",
				"Take;&aTook &6$<balance>&a from &6<name>&a's account.", "Give;&aGave &6$<balance>&a to &6<name>&a's account.", "Create;&aCreated account named &6<name>", "Pay;&aPaid &6<name> $<amount>", "ReceivePayment;&6<name>&7 paid you &6$<amount>",
				"Empty;&aDeleted all accounts", "Remove;&aDeleted &6<name>&a's account", "Loan;&aGot a loan of &6$<amount>", "PayOffLoan;&aPaid off &6$<amount>&a worth of loans owed", "Version;&7Installed version: &6<amount>",
				"VersionAuthors;&7Author(s): &6<name>", "VersionHelpers;&7Helper(s): &6<name>", "PayAll;&aPaid all users online &6$<balance>", "PayAllOffline;&aPaid all users &6$<balance>", "Purge;&aDeleted all accounts with the default balance.",
				"Transfer;&aTransferred &6$<balance>&a to &6<name>&a's account.", "Reload;&aReloaded iMonies.", "Convert;&aConverted all accounts to &6<name>", "Interest;&7You got &6$<balance>&7 for being online &6<amount>&7 seconds.", 
				"PutIntoBank;&aPut &6$<amount>&a into the bank", "TookFromBank;&aGot &6$<amount>&a from the bank", "MoneyInBank;&7You have &6$<balance>&7 in the bank.", "MustEnterDepositAmount;&7Please enter the amount to deposit into chat",
				"MustEnterWithdrawalAmount;&7Please enter the amount of money to withdraw into chat"};
		String[] allErrors = {"NoPermission;&4No permission", "NotEnoughMoney;&cNot enough money", "NegativeAmount;&cCan't be less than a cent", "NotEnoughArgs;&cNot enough arguments", "TooManyArgs;&cToo many arguments",
				"LoansNotEnabled;&cLoans are not enabled", "OverMaxLoanAmount;&cYou've reached your max loan amount. Pay off your loans!", "NotOwed;&cYou do not owe this amount of money", "NoAccount;&cThat account doesn't exist",
				"CannotConvert;&cYou can not convert between those two databases", "NotNumeric;&cValue must be a number!", "OverMoneyLimit;&cThat amount of money is not allowed.", "BanksNotEnabled;&cBanks are not enabled on this server",
				"MustUseSign;&cYou must use an iBank sign first"};
		String[] allHelp = {"Header;&6iMonies 3&7 -Commands", "Key;&7[] = optional, <> = required", "Money;&6/money&7 - Check your balance.", "MoneyOthers;&6/money&7 - Check your/others' balance", "MoneySet;&6/money set <name> <amount>&7 - Set someone's balance",
				"MoneyCreate;&6/money create <name>&7 - Create an account with the specified name", "MoneyHelp;&6/money help&7 - View all commands and usage", "MoneyPay;&6/money pay <name> <amount>&7 - Pay a player", "MoneyEmpty;&6/money empty&7 - Delete all accounts", "MoneyRemove;&6/money remove <name>&7 - Delete the specified account",
				"BalTop;&6/baltop&7 - Get the top balances", "MoneyVersion;&6/money version&7 - Check installed version of iMonies", "MoneyConvert;&6/money convert <essentials,iconomy,boseconomy>&7 - Convert from another economy plugin", "MoneyGive;&6/money give <name> <amount>&7 - Give an account money",
				"MoneyTake;&6/money take <name> <amount>&7 - Take money from an account", "MoneyLoan;&6/money <loan/payoffloan> <amount>&7 - Get or pay off a loan", "MoneyReload;&6/money reload&7 - Update changes in iMonies folder",
				"MoneySetOwes;&6/money setowes <name> <amount>&7 - Set one's amount owed", "MoneyPayAll;&6/money <payall/payalloffline> <amount>&7 - Pay all online (or online and offline) users", "MoneyPurge;&6/money purge&7 - Delete all accounts with default balance",
				"MoneyTransfer;&6/money transfer <from> <to> <amount>&7 - Transfer money between two accounts", "MoneyBank;&6/money bank <withdraw/deposit> <amount>&7 - Withdraw or deposit from the bank"};
		config.set("MustUseCommandWithdraw", null);
		config.save(configFile);
		config.set("MustUseCommandDeposit", null);
		config.save(configFile);
		for(String all : allMessages) {
			String[] parts = all.split("\\;");
			String message = parts[0];
			String defaultValue = parts[1];
			if(config.getString("Messages." + message) == null) {
				config.set("Messages." + message, defaultValue);
				config.save(configFile);
			}
		}
		for(String all : allErrors) {
			String[] parts = all.split("\\;");
			String message = parts[0];
			String defaultValue = parts[1];
			if(config.getString("Errors." + message) == null) {
				config.set("Errors." + message, defaultValue);
				config.save(configFile);
			}
		}
		for(String all : allHelp) {
			String[] parts = all.split("\\;");
			String message = parts[0];
			String defaultValue = parts[1];
			if(config.getString("Help." + message) == null) {
				config.set("Help." + message, defaultValue);
				config.save(configFile);
			}
		}
	}
	/**
	 * @param message
	 * @return Raw string from Messages file
	 */
	public String getMessage(String message) {
		return config.getString("Messages." + message);
	}
	/**
	 * @param message
	 * @return Raw string from Messages file
	 */
	public String getErrorMessage(String message) {
		return config.getString("Errors." + message);
	}
	/**
	 * @param message
	 * @return Raw string from Messages file
	 */
	public String getHelpMessage(String message) {
		return config.getString("Help." + message);
	}
}