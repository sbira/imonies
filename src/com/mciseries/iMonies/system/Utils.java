package com.mciseries.iMonies.system;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.api.Account;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.storage.Storage;

/**
 * @author rtainc
 *
 */
public class Utils {
	private static Messages msg = new Messages();
	private static Account api = new Account(Bukkit.getPluginManager().getPlugin("iMonies"));
	private static String[] playerNames = {"NPEfixer"};
	private static List<String> playerList = Arrays.asList(playerNames);
	
	public static double defaultBalance;
	public static boolean debug;
	public static String pluralName;
	public static String singularName;
	public static double moneyLimit;
	public static boolean useMySQL;
	public static String mySQLUsername;
	public static String mySQLPassword;
	public static String mySQLIP;
	public static String mySQLPort;
	public static String mySQLTable;
	public static String mySQLDatabase;
	public static String thousandsSeparator;
	public static String centsSeparator;
	public static boolean useLoans;
	public static boolean useBanks;
	public static double maxOwed;
	public static int topAccountsPerPage;
	public static boolean useBankCommands;
	public static boolean useBankSigns;
	public static boolean useInterest;
	public static boolean useInterestFromBank;
	public static double interestCutoff;
	public static double interestInterval;
	public static double interestAmount;
	public static boolean percentageInterest;
	public static boolean loseMoneyOnDeath;
	public static double signBankCreationCost;
	
	public static ArrayList<String> depositClickedPlayers = new ArrayList<String>(playerList);
	public static ArrayList<String> withdrawClickedPlayers = new ArrayList<String>(playerList);
	public static ArrayList<String> warnedUpdaters = new ArrayList<String>(playerList);
	
	public Utils() {}
	/**
	 * @param player The player
	 * @param perm The permission to check for
	 * @return Whether the player has the permission or not
	 */
	public static boolean hasPerms(String player, String perm) {
		Player s = Bukkit.getPlayer(player);
		if(s == null)
			return false;
		if(s.hasPermission("imonies." + perm)) {
			return true;
		}
		else {
			s.sendMessage(msg.getErrorMessage("NoPermission"));
			return false;
		}
	}
	/**
	 * @param player The player
	 * @param a The argument array
	 * @param num The amount of arguments needed
	 * @return Whether the player sent enough arguments or not
	 */
	public static boolean rightArgs(String player, String[] a, int num) {
		Player s = Bukkit.getPlayer(player);
		if(a.length == num)
			return true;
		else {
			if(a.length < num) {
				s.sendMessage(msg.getErrorMessage("NotEnoughArgs"));
				return false;
			}
			else {
				s.sendMessage(msg.getErrorMessage("TooManyArgs"));
				return false;
			}
		}
	}
	
	/**
	 * @param player The player
	 * @param name The account to check
	 * @return Whether the account exists or not
	 */
	public static boolean hasAccount(String player, String name) {
		if(APi.hasAccount(name)) {
			return true;
		}
		else {
			Bukkit.getPlayer(player).sendMessage(msg.getErrorMessage("NoAccount"));
			return false;
		}
	}
	
	/**
	 * @param player The player
	 * @param num The string to check
	 * @return Whether the string is a number or not
	 */
	public static boolean isNumeric(String player, String num) {
		Player s = Bukkit.getPlayer(player);
		if(api.isDouble(num)) {
			return true;
		}
		else {
			if(s == null)
				return false;
			s.sendMessage(msg.getErrorMessage("NotNumberic"));
			return false;
		}
	}
	
	/**
	 * @param player The player
	 * @param amount The amount to check for
	 * @return Whether the player has enough money or not
	 */
	public static boolean hasEnough(String player, double amount) {
		Player s = Bukkit.getPlayer(player);
		if(api.has(player, amount)) {
			return true;
		}
		else {
			if(s == null)
				return false;
			s.sendMessage(msg.getErrorMessage("NotEnoughMoney"));
			return false;
		}
	}
	
	/**
	 * @param money The NumberFormatted number to format
	 * @return The formatted money
	 */
	public static String formatMoney(String money) {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);
		String[] parts = nf.format(Double.parseDouble(money)).split("\\.");
		String dollars = parts[0];
		if(dollars.contains(","))
			dollars = dollars.replace(",", thousandsSeparator);
		String formatted = dollars + centsSeparator + parts[1];
		return formatted;
	}
	
	/**
	 * @param money The NumberFormatted number to format
	 * @return The formatted money
	 */
	public static String formatMoney(double money) {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);
		String[] parts = nf.format(money).split("\\.");
		String dollars = parts[0];
		if(dollars.contains(","))
			dollars = dollars.replace(",", thousandsSeparator);
		String formatted = dollars + centsSeparator + parts[1];
		return formatted;
	}
	
	/**
	 * @param w The world the sign is in
	 * @param x The x-coord of the sign
	 * @param y The y-coord of the sign
	 * @param z The z-coord of the sign
	 * @return Whether the sign is an iMonies Signbank or not
	 */
	public static boolean isSignBank(World w, String x, String y, String z) {
		Block b = w.getBlockAt(Integer.parseInt(x), Integer.parseInt(y), Integer.parseInt(z));
		if(((Sign)b.getState()).getLine(0).contains("[iBank]"))
			return true;
		else
			return false;
	}
	
	/**
	 * @param x The x-coord of the sign
	 * @param y The y-coord of the sign
	 * @param z The z-coord of the sign
	 * @param p The player creating the sign
	 */
	public static void newSignBank(String x, String y, String z, Player p) {
		Storage.update("INSERT INTO `signs` (x, y, z) VALUES (" + x + ", " + y + ", " + z + ");",
				"INSERT INTO `signs` (x, y, z) VALUES (" + x + ", " + y + ", " + z + ");");
	}
}
