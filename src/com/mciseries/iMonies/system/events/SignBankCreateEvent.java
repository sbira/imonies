package com.mciseries.iMonies.system.events;

import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author rtainc
 *
 */
public class SignBankCreateEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private String x;
	private String y;
	private String z;
	private Player p;
	private Block b;
	private boolean cancelled;
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param p
	 */
	public SignBankCreateEvent(String x, String y, String z, Player p) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.p = p;
		this.b = p.getWorld().getBlockAt(getX(), getY(), getZ());
	}
	/* (non-Javadoc)
	 * @see org.bukkit.event.Event#getHandlers()
	 */
	public HandlerList getHandlers() {
		return handlers;
	}
	/**
	 * @return The handlers registered for this event
	 */
	public static HandlerList getHandlerList() {
		return handlers;
	}
	/**
	 * @return The player who created the bank
	 */
	public Player getPlayer() {
		return p;
	}
	/**
	 * @return The bank block
	 */
	public Block getBlock() {
		return b;
	}
	/**
	 * @return The bank block (in Sign form)
	 */
	public Sign getSign() {
		return (Sign)b.getState();
	}
	/**
	 * @return The bank block's X coordinate
	 */
	public int getX() {
		return Integer.parseInt(x);
	}
	/**
	 * @return The bank block's Y coordinate
	 */
	public int getY() {
		return Integer.parseInt(y);
	}
	/**
	 * @return The bank block's Z coordinate
	 */
	public int getZ() {
		return Integer.parseInt(z);
	}
	/* (non-Javadoc)
	 * @see org.bukkit.event.Cancellable#isCancelled()
	 */
	public boolean isCancelled() {
		return cancelled;
	}
	/* (non-Javadoc)
	 * @see org.bukkit.event.Cancellable#setCancelled(boolean)
	 */
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}
