package com.mciseries.iMonies.system.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.mciseries.iMonies.api.Account;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Sign implements Listener {
	private Messages msg = new Messages();
	private Account api = new Account(Bukkit.getPluginManager().getPlugin("iMonies"));
	/**
	 * @param e
	 */
	@EventHandler
	public void onSignChangeEvent(SignChangeEvent e) {
		String[] lines = e.getLines();
		String x = "" + e.getBlock().getLocation().getBlockX();
		String y = "" + e.getBlock().getLocation().getBlockY();
		String z = "" + e.getBlock().getLocation().getBlockZ();
		if(lines[0].equalsIgnoreCase("[iBank]") && Utils.useBankSigns && Utils.useBanks && Utils.hasPerms(e.getPlayer().getName(), "signbank")) {
			if(Utils.hasEnough(e.getPlayer().getName(), Utils.signBankCreationCost)) {
				api.withdrawPlayer(e.getPlayer().getName(), Utils.signBankCreationCost);
				Utils.newSignBank(x, y, z, e.getPlayer());
				e.setLine(0, ChatColor.DARK_GREEN + "[iBank]");
				e.getPlayer().sendMessage("Charged %s for a sign creation.");
			}
		}
	}
	/**
	 * @param e
	 */
	@EventHandler
	public void onBlockBreakEvent(BlockBreakEvent e) {
		String x = "" + e.getBlock().getX();
		String y = "" + e.getBlock().getY();
		String z = "" + e.getBlock().getZ();
		if((e.getBlock().getType() == Material.WALL_SIGN || e.getBlock().getType() == Material.SIGN || e.getBlock().getType() == Material.SIGN_POST) && Utils.isSignBank(e.getBlock().getWorld(), x, y, z)) {
			// delete
		}
	}
	/**
	 * @param e
	 */
	@EventHandler
	public void onPlayerInteractEvent(PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			String x = "" + e.getClickedBlock().getX();
			String y = "" + e.getClickedBlock().getY();
			String z = "" + e.getClickedBlock().getZ();
			if((e.getClickedBlock().getType() == Material.WALL_SIGN || e.getClickedBlock().getType() == Material.SIGN || e.getClickedBlock().getType() == Material.SIGN_POST) && Utils.isSignBank(e.getClickedBlock().getWorld(), x, y, z)) {
				Utils.depositClickedPlayers.add(e.getPlayer().getName());
				deposit(e.getPlayer());
			}
		}
		else if(e.getAction() == Action.LEFT_CLICK_BLOCK) {
			String x = "" + e.getClickedBlock().getX();
			String y = "" + e.getClickedBlock().getY();
			String z = "" + e.getClickedBlock().getZ();
			if((e.getClickedBlock().getType() == Material.WALL_SIGN || e.getClickedBlock().getType() == Material.SIGN || e.getClickedBlock().getType() == Material.SIGN_POST) && Utils.isSignBank(e.getClickedBlock().getWorld(), x, y, z)) {
				Utils.withdrawClickedPlayers.add(e.getPlayer().getName());
				withdraw(e.getPlayer());
			}
		}
	}
	/**
	 * @param p
	 */
	public void deposit(Player p) {
		p.sendMessage(msg.getMessage("MustEnterDepositAmount", "", "", ""));
	}
	/**
	 * @param p
	 */
	public void withdraw(Player p) {
		p.sendMessage(msg.getMessage("MustEnterWithdrawalAmount", "", "", ""));
	}
}
