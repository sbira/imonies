package com.mciseries.iMonies.system.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.mciseries.iMonies.api.APi;

/**
 * @author rtainc
 *
 */
public class Join implements Listener {
	/**
	 * @param e
	 */
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		if(!APi.hasAccount(e.getPlayer().getName()))
			APi.createAccount(e.getPlayer().getName());
	}
}
