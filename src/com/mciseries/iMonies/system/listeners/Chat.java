package com.mciseries.iMonies.system.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

public class Chat implements Listener {
	private Messages msg = new Messages();
	@EventHandler
	public void onAsyncPlayerChatEvent(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		if(Utils.depositClickedPlayers.contains(e.getPlayer().getName()) || Utils.withdrawClickedPlayers.contains(e.getPlayer().getName())) {
			if(Utils.isNumeric(e.getPlayer().getName(), e.getMessage())) {
				if(Utils.depositClickedPlayers.contains(e.getPlayer().getName())) {
					if(APi.has(e.getPlayer().getName(), Double.parseDouble(e.getMessage()))) {
						APi.payBank(e.getPlayer().getName(), Double.parseDouble(e.getMessage()), true);
						Utils.depositClickedPlayers.remove(e.getPlayer().getName());
						p.sendMessage(msg.getMessage("PutIntoBank", "", Utils.formatMoney(e.getMessage()), ""));
					}
					else {
						p.sendMessage(msg.getErrorMessage("NotEnoughMoney"));
						Utils.depositClickedPlayers.remove(e.getPlayer().getName());
					}
				}
				else {
					if(APi.bankHas(e.getPlayer().getName(), Double.parseDouble(e.getMessage()))) {
						APi.chargeBank(e.getPlayer().getName(), Double.parseDouble(e.getMessage()), true);
						Utils.withdrawClickedPlayers.remove(e.getPlayer().getName());
						p.sendMessage(msg.getMessage("TookFromBank", "", Utils.formatMoney(e.getMessage()), ""));
					}
					else {
						p.sendMessage(msg.getErrorMessage("NotEnoughMoney"));
						Utils.withdrawClickedPlayers.remove(e.getPlayer().getName());
					}
				}
				
			}
			e.setCancelled(true);
		}
	}
}
