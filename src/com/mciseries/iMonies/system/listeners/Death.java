package com.mciseries.iMonies.system.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.mciseries.iMonies.api.Account;
import com.mciseries.iMonies.system.Utils;
import com.mciseries.iMonies.system.files.Config;

/**
 * @author rtainc
 *
 */
public class Death implements Listener {
	Account account = new Account(Bukkit.getPluginManager().getPlugin("iMonies"));
	Config config = new Config();
	/**
	 * @param e
	 */
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		Player player = e.getEntity();
		if(account.hasAccount(player.getName())) {
			if(Utils.loseMoneyOnDeath)
				account.setBalance(player.getName(), 0.0);
		}
	}
}
