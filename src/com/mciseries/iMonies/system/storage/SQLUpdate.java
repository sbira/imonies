package com.mciseries.iMonies.system.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.concurrent.Callable;

import com.mciseries.iMonies.iMonies;
import com.mciseries.iMonies.system.Utils;

public class SQLUpdate implements Callable<Object> {
	private String query;
	private Connection conn;
	
	public SQLUpdate(String query, String liteQuery) {
		if(Utils.useMySQL)
			this.query = query;
		else
			this.query = liteQuery;
	}
	
	@Override
	public Object call() throws Exception {
		if(Utils.useMySQL) {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://" + Utils.mySQLIP + ":" + Utils.mySQLPort + "/" + Utils.mySQLDatabase, Utils.mySQLUsername, Utils.mySQLPassword);
		}
		else {
			Class.forName("org.sqlite.JDBC").newInstance();
			conn = DriverManager.getConnection("jdbc:sqlite:plugins/iMonies/Accounts.db");
		}
		Statement state = conn.createStatement();
		state.executeUpdate(query);
		if(Utils.debug)
			new iMonies().log("Did query: " + query, true);
		state.close();
		conn.close();
		return null;
	}
}
