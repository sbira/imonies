package com.mciseries.iMonies.system.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.Callable;

import com.mciseries.iMonies.iMonies;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class SQLQuery implements Callable<Object> {
	private String query;
	private Connection conn;
	private String column;
	private Object res;
	
	public SQLQuery(String query, String liteQuery, String column) {
		this.column = column;
		if(Utils.useMySQL)
			this.query = query;
		else
			this.query = liteQuery;
	}
	
	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Object call() throws Exception {
		if(Utils.useMySQL) {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://" + Utils.mySQLIP + ":" + Utils.mySQLPort + "/" + Utils.mySQLDatabase, Utils.mySQLUsername, Utils.mySQLPassword);
		}
		else {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:plugins/iMonies/Accounts.db");
		}
		Statement state = conn.createStatement();
		ResultSet result = state.executeQuery(query);
		if(Utils.debug)
			new iMonies().log("Did query: " + query, true);
		if(result.next())
			res = result.getObject(column);
		state.close();
		result.close();
		return res;
	}
}
