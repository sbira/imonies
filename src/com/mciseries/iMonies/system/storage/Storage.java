package com.mciseries.iMonies.system.storage;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Storage {
	private static ExecutorService pool = Executors.newFixedThreadPool(3);
	public static void update(String query, String liteQuery) {
		Callable<Object> call = new SQLUpdate(query, liteQuery);
		@SuppressWarnings("unused")
		Future<Object> future = pool.submit(call);
	}
	public static Object get(String query, String liteQuery, String column) {
		Set<Future<Object>> set = new HashSet<Future<Object>>();
		Callable<Object> call = new SQLQuery(query, liteQuery, column);
		Future<Object> future = pool.submit(call);
		set.add(future);
		Object obj = null;
		for(Future<Object> fut : set) {
			try {
				obj = fut.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		return obj;
	}
	public static boolean rowExists(String name) {
		Set<Future<Object>> set = new HashSet<Future<Object>>();
		Callable<Object> call = new SQLRows(name);
		Future<Object> future = pool.submit(call);
		set.add(future);
		Object obj = null;
		for(Future<Object> fut : set) {
			try {
				obj = fut.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		return (Boolean) obj;
	}
	public static void initialize() {
		update("CREATE TABLE IF NOT EXISTS `accounts` ( `id` int(255) NOT NULL AUTO_INCREMENT, `name` varchar(32) NOT NULL, `balance` double(64,2) NOT NULL DEFAULT 0.00, `owes` double(64,2) NOT NULL DEFAULT 0.00, UNIQUE KEY `name` (`name`), PRIMARY KEY `id` (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
				"CREATE TABLE IF NOT EXISTS `accounts` ( `name` varchar(32) NOT NULL, `balance` double(64,2) NOT NULL, `owes` double(64,2) NOT NULL DEFAULT 0.00 );");
		update("CREATE TABLE IF NOT EXISTS `bank` ( `id` int(255) NOT NULL AUTO_INCREMENT, `name` varchar(32) NOT NULL, `balance` double(64,2) NOT NULL DEFAULT 0.00, UNIQUE KEY `name` (`name`), PRIMARY KEY `id` (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
				"CREATE TABLE IF NOT EXISTS `bank` ( `name` varchar(32) NOT NULL, `balance` double(64,2) NOT NULL );");
		update("CREATE TABLE IF NOT EXISTS `signs` ( `id` int(255) NOT NULL AUTO_INCREMENT, `x` int(6) NOT NULL, `y` int(2) NOT NULL, `z` int(6) NOT NULL, PRIMARY KEY `id` (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
				"CREATE TABLE IF NOT EXISTS `signs` ( `x` int(6) NOT NULL, `y` int(2) NOT NULL, `z` int(6) NOT NULL );");
	}
}
