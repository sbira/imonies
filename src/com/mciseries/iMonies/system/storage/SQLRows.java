package com.mciseries.iMonies.system.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.Callable;

import com.mciseries.iMonies.iMonies;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class SQLRows implements Callable<Object> {
	private Connection conn;
	private String query;
	
	public SQLRows(String name) {
		query = "SELECT * FROM `accounts` WHERE `name`='" + name + "';";
	}
	
	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Object call() throws Exception {
		if(Utils.useMySQL) {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://" + Utils.mySQLIP + ":" + Utils.mySQLPort + "/" + Utils.mySQLDatabase, Utils.mySQLUsername, Utils.mySQLPassword);
		}
		else {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:plugins/iMonies/Accounts.db");
		}
		Statement state = conn.createStatement();
		ResultSet result = state.executeQuery(query);
		if(Utils.debug)
			new iMonies().log("Did query: " + query, true);
		int rows = 0;
		while(result.next()) {
			rows++;
		}
		boolean exists;
		if(rows == 0)
			exists = false;
		else
			exists = true;
		state.close();
		result.close();
		return exists;
	}
}