package com.mciseries.iMonies;

import org.bukkit.plugin.java.JavaPlugin;

import com.mciseries.iMonies.system.Utils;
import com.mciseries.iMonies.system.startup.*;

/**
 * @author rtainc
 *
 */
public class iMonies extends JavaPlugin {
	/* (non-Javadoc)
	 * @see org.bukkit.plugin.java.JavaPlugin#onEnable()
	 */
	public void onEnable() {
		Long s = System.nanoTime(); log("Set StartTime to " + s, true);
		new CreateFiles(); log("Created/Updated Config, Accounts, and Messages", true);
		new MetricsData(this); log("Initialized Metrics", true);
		new RegisterCommands(this); log("Registering commands with Bukkit", true);
		new VaultHooking(this).hook(); log("Opening connection to Vault", true);
		new RegisterEvents(this); log("Registering PlayerJoinEvent", true);
		Long e = System.nanoTime(); log("Set EndTime to " + e, true);
		Long ns = e - s; log("Subtracted StartTime from EndTime", true);
		Long ms = ns / 1000000; log("Converted time from " + ns + " nanoseconds to " + ms + " miliseconds", true);
		log("Took " + ms + "ms to start up.", true);
	}
	/* (non-Javadoc)
	 * @see org.bukkit.plugin.java.JavaPlugin#onDisable()
	 */
	public void onDisable() {
		new VaultHooking(this).unhook();
	}
	/**
	 * @param message
	 * @param debug
	 */
	public void log(String message, boolean debug) {
		if(debug) {
			if(Utils.debug) {
				getLogger().info(message);
			}
		}
		else {
			getLogger().info(message);
		}
	}
}