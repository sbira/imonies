package com.mciseries.iMonies.api;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.Plugin;

import com.mciseries.iMonies.iMonies;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;
import com.mciseries.iMonies.system.storage.Storage;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

/**
 * @author rtainc
 *
 */
public class Account implements Economy {
	Messages messages = new Messages();
	private final String name = "iMonies";
	private Plugin plugin = null;
	private iMonies imonies = null;
	/**
	 * @param plugin
	 */
	public Account(Plugin plugin) {
		this.plugin = plugin;
		if(imonies == null) {
			Plugin imon = plugin.getServer().getPluginManager().getPlugin("iMonies");
			if(imon != null && imon.isEnabled()) {
				imonies = (iMonies) imon;
			}
		}
	}
	
	public class EconomyServerListener implements Listener {
		Account economy = null;
		
		/**
		 * @param economy_iMonies
		 */
		public EconomyServerListener(Account economy_iMonies) {
			this.economy = economy_iMonies;
		}
		
		/**
		 * @param event
		 */
		@EventHandler(priority = EventPriority.MONITOR)
		public void onPluginEnable(PluginEnableEvent event) {
			if(economy.imonies == null) {
				Plugin imon = plugin.getServer().getPluginManager().getPlugin("iMonies");
				
				if(imon != null && imon.isEnabled()) {
					economy.imonies = (iMonies) imon;
					imonies.log("Hooked into Vault", true);
				}
			}
		}
		
		/**
		 * @param event
		 */
		@EventHandler(priority = EventPriority.MONITOR)
		public void onPluginDisable(PluginDisableEvent event) {
			if(economy.imonies != null) {
				if(event.getPlugin().getDescription().getName().equals("iMonies")) {
					economy.imonies = null;
					imonies.log("Unhooked from Vault", true);
				}
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		return imonies != null;
	}
	
	/**
	 * @param value
	 * @return Whether the String is a number or not
	 */
	public boolean isDouble(String value) {
		try {
			Double.parseDouble(value);
			return true;
		}
		catch(NumberFormatException e) {
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
	
	/**
	 * @param playerName
	 * @param amount
	 */
	public void setBalance(String playerName, double amount) {
		Storage.update("UPDATE `accounts` SET `balance`=" + amount + " WHERE `name`='" + playerName.toLowerCase() + "'",
				"UPDATE `accounts` SET `balance`=" + amount + " WHERE `name`='" + playerName.toLowerCase() + "'");
	}
	
	/**
	 * @param playerName
	 * @param amount
	 */
	public void setOwes(String playerName, double amount) {
		Storage.update("UPDATE `accounts` SET `owes`=" + amount + " WHERE `name`='" + playerName.toLowerCase() + "'",
				"UPDATE `accounts` SET `owes`=" + amount + " WHERE `name`='" + playerName.toLowerCase() + "'");
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#hasBankSupport()
	 */
	@Override
	public boolean hasBankSupport() {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#fractionalDigits()
	 */
	@Override
	public int fractionalDigits() {
		return 2;
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#format(double)
	 */
	@Override
	public String format(double amount) {
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(amount);
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#currencyNamePlural()
	 */
	@Override
	public String currencyNamePlural() {
		return Utils.pluralName;
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#currencyNameSingular()
	 */
	@Override
	public String currencyNameSingular() {
		return Utils.singularName;
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#hasAccount(java.lang.String)
	 */
	@Override
	public boolean hasAccount(String playerName) {
		if(Storage.rowExists(playerName))
			return true;
		else
			return false;
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#getBalance(java.lang.String)
	 */
	@Override
	public double getBalance(String playerName) {
		return (Double) Storage.get("SELECT * FROM `accounts` WHERE `name`='" + playerName.toLowerCase() + "'",
				"SELECT * FROM `accounts` WHERE `name`='" + playerName.toLowerCase() + "'", "balance");
	}
	
	/**
	 * @param playerName
	 * @return Amount of money a player owes
	 */
	public double getOwes(String playerName) {
		return (Double) Storage.get("SELECT * FROM `accounts` WHERE `name`='" + playerName.toLowerCase() + "'",
				"SELECT * FROM `accounts` WHERE `name`='" + playerName.toLowerCase() + "'", "owes");
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#has(java.lang.String, double)
	 */
	@Override
	public boolean has(String playerName, double amount) {
		if(getBalance(playerName) >= amount) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#withdrawPlayer(java.lang.String, double)
	 */
	@Override
	public EconomyResponse withdrawPlayer(String playerName, double amount) {
		setBalance(playerName, getBalance(playerName) - amount);
		return new EconomyResponse(amount, getBalance(playerName), ResponseType.SUCCESS, null);
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#depositPlayer(java.lang.String, double)
	 */
	@Override
	public EconomyResponse depositPlayer(String playerName, double amount) {
		if(getBalance(playerName) + amount > Utils.moneyLimit) {
			if(Bukkit.getPlayer(playerName) != null) {
				Bukkit.getPlayer(playerName).sendMessage(messages.getErrorMessage("OverMoneyLimit"));
			}
			return new EconomyResponse(amount, getBalance(playerName), ResponseType.FAILURE, null);
		}
		else {
			setBalance(playerName, getBalance(playerName) + amount);
		}
		return new EconomyResponse(amount, getBalance(playerName), ResponseType.SUCCESS, null);
	}
	
	/**
	 * @param playerName
	 * @param amount
	 * @return Whether the transaction was successful or not
	 */
	public EconomyResponse withdrawOwes(String playerName, double amount) {
		setOwes(playerName, getOwes(playerName) - amount);
		return new EconomyResponse(amount, getOwes(playerName), ResponseType.SUCCESS, null);
	}
	
	/**
	 * @param playerName
	 * @param amount
	 * @return Whether the transaction was successful or not
	 */
	public EconomyResponse depositOwes(String playerName, double amount) {
		if(getOwes(playerName) + amount > Utils.moneyLimit) {
			return new EconomyResponse(amount, getOwes(playerName), ResponseType.FAILURE, null);
		}
		else {
			setOwes(playerName, getOwes(playerName) + amount);
		}
		return new EconomyResponse(amount, getOwes(playerName), ResponseType.SUCCESS, null);
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#createBank(java.lang.String, java.lang.String)
	 */
	@Override
	public EconomyResponse createBank(String name, String player) {
		return new EconomyResponse(0,0, ResponseType.NOT_IMPLEMENTED, "iMonies doesn't support banks!");
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#deleteBank(java.lang.String)
	 */
	@Override
	public EconomyResponse deleteBank(String name) {
		return new EconomyResponse(0,0, ResponseType.NOT_IMPLEMENTED, "iMonies doesn't support banks!");
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#bankBalance(java.lang.String)
	 */
	@Override
	public EconomyResponse bankBalance(String name) {
		return new EconomyResponse(0,0, ResponseType.NOT_IMPLEMENTED, "iMonies doesn't support banks!");
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#bankHas(java.lang.String, double)
	 */
	@Override
	public EconomyResponse bankHas(String name, double amount) {
		return new EconomyResponse(0,0, ResponseType.NOT_IMPLEMENTED, "iMonies doesn't support banks!");
	}
	
	/* (non-Javadoc)
	 * @see net.milkbowl.vault.economy.Economy#bankWithdraw(java.lang.String, double)
	 */
	@Override
		public EconomyResponse bankWithdraw(String name, double amount) {
		return new EconomyResponse(0,0, ResponseType.NOT_IMPLEMENTED, "iMonies doesn't support banks!");
		}

		/* (non-Javadoc)
		 * @see net.milkbowl.vault.economy.Economy#bankDeposit(java.lang.String, double)
		 */
		@Override
		public EconomyResponse bankDeposit(String name, double amount) {
			return new EconomyResponse(0,0, ResponseType.NOT_IMPLEMENTED, "iMonies doesn't support banks!");
		}

		/* (non-Javadoc)
		 * @see net.milkbowl.vault.economy.Economy#isBankOwner(java.lang.String, java.lang.String)
		 */
		@Override
		public EconomyResponse isBankOwner(String name, String playerName) {
			return new EconomyResponse(0,0, ResponseType.NOT_IMPLEMENTED, "iMonies doesn't support banks!");
		}

		/* (non-Javadoc)
		 * @see net.milkbowl.vault.economy.Economy#isBankMember(java.lang.String, java.lang.String)
		 */
		@Override
		public EconomyResponse isBankMember(String name, String playerName) {
			return new EconomyResponse(0,0, ResponseType.NOT_IMPLEMENTED, "iMonies doesn't support banks!");
		}

		/* (non-Javadoc)
		 * @see net.milkbowl.vault.economy.Economy#getBanks()
		 */
		@Override
		public List<String> getBanks() {
			return new ArrayList<String>();
		}

		/* (non-Javadoc)
		 * @see net.milkbowl.vault.economy.Economy#createPlayerAccount(java.lang.String)
		 */
		@Override
		public boolean createPlayerAccount(String playerName) {
			if(!hasAccount(playerName))
				Storage.update("INSERT INTO `accounts` (name, balance) VALUES ('" + playerName + "', " + Utils.defaultBalance + ");",
						"INSERT INTO `accounts` (name, balance) VALUES ('" + playerName + "', " + Utils.defaultBalance + ");");
			return true;
		}
}