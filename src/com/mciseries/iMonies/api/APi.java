package com.mciseries.iMonies.api;

import org.bukkit.Bukkit;

/**
 * @author rtainc
 *
 */
public class APi {
	private static Bank bapi = new Bank();
	private static Account api = new Account(Bukkit.getPluginManager().getPlugin("iMonies"));
	public APi() {}
	/**
	 * @param name Player name
	 * @param amount Amount to charge
	 */
	public static void charge(String name, double amount) {
		api.withdrawPlayer(name, amount);
	}
	/**
	 * @param name Player name
	 * @param amount Amount to give
	 */
	public static void pay(String name, double amount) {
		api.depositPlayer(name, amount);
	}
	/**
	 * @param name Player name
	 * @param amount Amount to set to
	 */
	public static void setBalance(String name, double amount) {
		api.setBalance(name, amount);
	}
	/**
	 * @param name Player name
	 * @return The balance of the player
	 */
	public static double getBalance(String name) {
		return api.getBalance(name);
	}
	/**
	 * @param name Player name
	 * @param amount Amount to check for
	 * @return Whether the player has enough or not
	 */
	public static boolean has(String name, double amount) {
		return api.has(name, amount);
	}
	/**
	 * @param name Player name
	 * @return Amount of money in the bank
	 */
	public static double getBankBalance(String name) {
		return bapi.getBalance(name);
	}
	/**
	 * @param name Player name
	 * @param amount Amount of money to have in the bank
	 */
	public static void setBankBalance(String name, double amount) {
		bapi.setBalance(name, amount);
	}
	/**
	 * @param name Player name
	 * @param amount Amount of money to put in bank
	 * @param takeFromPlayer Whether to take that money from the player or not
	 */
	public static void payBank(String name, double amount, boolean takeFromPlayer) {
		bapi.depositBalance(name, amount);
		if(takeFromPlayer)
			api.withdrawPlayer(name, amount);
	}
	/**
	 * @param name Player name
	 * @param amount Amount of money to take from bank
	 * @param addToPlayer Whether to give that money to the player or not
	 */
	public static void chargeBank(String name, double amount, boolean addToPlayer) {
		bapi.withdrawBalance(name, amount);
		if(addToPlayer)
			api.depositPlayer(name, amount);
	}
	/**
	 * @param name Player name
	 * @return Amount owed in loans
	 */
	public static double getOwed(String name) {
		return api.getOwes(name);
	}
	/**
	 * @param name Player name
	 * @param amount Amount to owe in loans
	 */
	public static void setOwed(String name, double amount) {
		api.setOwes(name, amount);
	}
	/**
	 * @param name Player name
	 * @param amount Amount more to owe
	 * @param addToPlayer Whether to add a loan to the player or not
	 */
	public static void addOwed(String name, double amount, boolean addToPlayer) {
		if(addToPlayer)
			api.depositPlayer(name, amount);
		api.withdrawOwes(name, amount);
	}
	/**
	 * @param name Player name
	 * @param amount Amount less to owe
	 * @param takeFromPlayer Whether to remove from the loans the player has to pay off or not
	 */
	public static void removeOwed(String name, double amount, boolean takeFromPlayer) {
		if(takeFromPlayer)
			api.withdrawPlayer(name, amount);
		api.depositOwes(name, amount);
	}
	
	/**
	 * @param name Player name
	 * @param amount Amount to check for
	 * @return Whether this amount of money is in the bank or not
	 */
	public static boolean bankHas(String name, double amount) {
		return bapi.getBalance(name) >= amount;
	}
	
	/**
	 * @param name Player name
	 */
	public static void createAccount(String name) {
		api.createPlayerAccount(name);
	}
	
	/**
	 * @param name Player name
	 * @return Whether the player has an account or not
	 */
	public static boolean hasAccount(String name) {
		return api.hasAccount(name);
	}
}
