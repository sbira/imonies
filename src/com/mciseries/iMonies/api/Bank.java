package com.mciseries.iMonies.api;

import com.mciseries.iMonies.system.storage.Storage;


/**
 * @author rtainc
 *
 */
public class Bank {
	public Bank() {}
	/**
	 * @param playerName
	 * @return The amount of money in the bank for the player
	 */
	public double getBalance(String name) {
		return (Double) Storage.get("SELECT * FROM `bank` WHERE `name`='" + name.toLowerCase() + "';",
				"SELECT * FROM `bank` WHERE `name`='" + name.toLowerCase() + "';", "balance");
	}
	/**
	 * @param name
	 * @param amount
	 */
	public void setBalance(String name, double amount) {
		Storage.update("INSERT INTO `bank` (name, balance) VALUES ('" + name.toLowerCase() + "', " + amount + ") ON DUPLICATE KEY UPDATE `balance`=" + amount,
				"INSERT OR IGNORE INTO `bank` (name, balance) VALUES ('" + name.toLowerCase() + "', " + amount + "); UPDATE `bank` SET `balance`=" + amount + " WHERE `name`=" + name.toLowerCase());
	}
	/**
	 * @param name
	 * @param amount
	 */
	public void withdrawBalance(String name, double amount) {
		setBalance(name, getBalance(name) - amount);
	}
	/**
	 * @param name
	 * @param amount
	 */
	public void depositBalance(String name, double amount) {
		setBalance(name, getBalance(name) + amount);
	}
}
