package com.mciseries.iMonies.handlers.bank;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.api.Account;
import com.mciseries.iMonies.api.Bank;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Deposit {
	private Account api = new Account(Bukkit.getPluginManager().getPlugin("iMonies"));
	private Bank bapi = new Bank();
	private Messages msg = new Messages();
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Deposit(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s.getName(), "bank")) {
			if(Utils.useBanks) {
				if(!(Utils.useBankCommands)) {
					s.sendMessage(msg.getErrorMessage("MustUseSign"));
					return;
				}
				if(Utils.isNumeric(s.getName(), a[2])) {
					if(api.has(s.getName(), Double.parseDouble(a[2]))) {
						api.withdrawPlayer(s.getName(), Double.parseDouble(a[2]));
						bapi.depositBalance(s.getName(), Double.parseDouble(a[2]));
						s.sendMessage(msg.getMessage("PutIntoBank", "", Utils.formatMoney(a[2]), ""));
					}
					else {
						s.sendMessage(msg.getErrorMessage("NotEnoughMoney"));
					}
				}
			}
			else {
				s.sendMessage(msg.getErrorMessage("BanksNotEnabled"));
			}
		}
	}
}
