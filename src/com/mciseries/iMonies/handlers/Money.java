package com.mciseries.iMonies.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.api.Bank;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Money {
	private Bank bapi = new Bank();
	private Messages messages = new Messages();
	
	/**
	 * @param s
	 * @param c
	 * @param a
	 * @param other
	 */
	public Money(CommandSender s, Command c, String[] a, boolean other) {
		if(!other) {
			if(!(s instanceof Player)) {
				s.sendMessage(messages.getErrorMessage("NotEnoughArgs"));
			}
			else {
				if(APi.getOwed(s.getName()) > 0)
					s.sendMessage(messages.getMessage("OwesMoney", "", "", Utils.formatMoney(APi.getOwed(s.getName()))));
				s.sendMessage(messages.getMessage("Money", "", "", Utils.formatMoney(APi.getBalance(s.getName()))));
				if(Utils.useBanks && bapi.getBalance(s.getName()) > 0)
					s.sendMessage(messages.getMessage("MoneyInBank", "", "", Utils.formatMoney(bapi.getBalance(s.getName()))));
			}
		}
		else {
			if(Utils.hasPerms(s.getName(), "balance.others")) {
				if(Utils.hasAccount(s.getName(), a[0])) {
					s.sendMessage(messages.getMessage("MoneyOthers", a[0], "", Utils.formatMoney(APi.getBalance(a[0]))));
				}
			}
		}
	}
}
