package com.mciseries.iMonies.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Reload {
	Messages messages = new Messages();
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Reload(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s.getName(), "reload")) {
			if(Utils.rightArgs(s.getName(), a, 1)) {
				if(!(Bukkit.getPluginManager().getPlugin("iMonies") == null)) {
					Bukkit.getPluginManager().disablePlugin(Bukkit.getPluginManager().getPlugin("iMonies"));
					Bukkit.getPluginManager().enablePlugin(Bukkit.getPluginManager().getPlugin("iMonies"));
				}
				s.sendMessage(messages.getMessage("Reload", "", "", ""));
			}
		}
	}
}
