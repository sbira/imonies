package com.mciseries.iMonies.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author iVector
 *
 */
public class About {
	private Messages messages = new Messages();
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public About(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s.getName(), "version")) {
			if(Utils.rightArgs(s.getName(), a, 1)) {
				s.sendMessage(messages.getMessage("Version", "", Bukkit.getPluginManager().getPlugin("iMonies").getDescription().getVersion(), ""));
				s.sendMessage(messages.getMessage("VersionAuthors", "rtainc, iVector", "", ""));
				s.sendMessage(messages.getMessage("VersionHelpers", "x_clucky", "", ""));
			}
		}
	}
}
