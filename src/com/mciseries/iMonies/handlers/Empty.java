package com.mciseries.iMonies.handlers;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;
import com.mciseries.iMonies.system.files.Config;
import com.mciseries.iMonies.system.storage.Storage;

/**
 * @author rtainc
 *
 */
public class Empty {
	Messages messages = new Messages();
	Config config = new Config();
	
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Empty(CommandSender s, Command c, String[] a) {
		if(Utils.rightArgs(s.getName(), a, 1)) {
			if(Utils.hasPerms(s.getName(), "empty")) {
				if(!Utils.useMySQL) {
					File file = new File("plugins" + File.separator + "iMonies" + File.separator + "accounts.db");
					if(file.exists())
						file.delete();
				}
				else {
					Storage.update("DROP TABLE IF EXISTS `accounts`", "");
				}
				Bukkit.getPluginManager().disablePlugin(Bukkit.getPluginManager().getPlugin("iMonies"));
				Bukkit.getPluginManager().enablePlugin(Bukkit.getPluginManager().getPlugin("iMonies"));
				
				s.sendMessage(messages.getMessage("Empty", "", "", ""));
			}
		}
	}
}
