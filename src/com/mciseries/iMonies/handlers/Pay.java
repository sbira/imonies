package com.mciseries.iMonies.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Pay {
	private Messages messages = new Messages();
	
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Pay(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s.getName(), "pay")) {
			if(Utils.rightArgs(s.getName(), a, 3)) {
				if(Utils.isNumeric(s.getName(), a[2])) {
					if(Utils.hasAccount(s.getName(), a[1])) {
						if(Double.parseDouble(a[2]) <= APi.getBalance(s.getName()) && Double.parseDouble(a[2]) >= 0.01) {
							APi.charge(s.getName(), Double.parseDouble(a[2]));
							APi.pay(a[1], Double.parseDouble(a[2]));
							s.sendMessage(messages.getMessage("Pay", a[1], Utils.formatMoney(a[2]), Utils.formatMoney(a[2])));
							if(Bukkit.getPlayer(a[1]) == null) {}
							else
								Bukkit.getPlayer(a[1]).sendMessage(messages.getMessage("ReceivePayment", s.getName(), Utils.formatMoney(a[2]), Utils.formatMoney(a[2])));
						}
						else {
							s.sendMessage(messages.getErrorMessage("NotEnoughMoney"));
						}
					}
				}
			}
		}
	}
}
