package com.mciseries.iMonies.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class SetOwes {
	private Messages messages = new Messages();
	
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public SetOwes(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s.getName(), "set")) {
			if(Utils.rightArgs(s.getName(), a, 3)) {
				if(Utils.isNumeric(s.getName(), a[2])) {
					if(Utils.hasAccount(s.getName(), a[1])) {
						APi.setOwed(a[1], Double.parseDouble(a[2]));
						s.sendMessage(messages.getMessage("SetOwes", a[1], "", Utils.formatMoney(a[2])));
					}
				}
			}
		}
	}
}
