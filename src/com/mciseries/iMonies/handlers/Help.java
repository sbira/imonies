package com.mciseries.iMonies.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Help {
	private Messages msg = new Messages();
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Help(CommandSender s, Command c, String[] a) {
		boolean others = s.hasPermission("imonies.balance.others");
		s.sendMessage(msg.getHelpMessage("Header"));
		s.sendMessage(msg.getHelpMessage("Key"));
		if(others)
			s.sendMessage(msg.getHelpMessage("MoneyOthers"));
		else
			s.sendMessage(msg.getHelpMessage("Money"));
		if(s.hasPermission("imonies.help"))
			s.sendMessage(msg.getHelpMessage("MoneyHelp"));
		if(s.hasPermission("imonies.set"))
			s.sendMessage(msg.getHelpMessage("MoneySet"));
		if(s.hasPermission("imonies.create"))
			s.sendMessage(msg.getHelpMessage("MoneyCreate"));
		if(s.hasPermission("imonies.pay"))
			s.sendMessage(msg.getHelpMessage("MoneyPay"));
		if(s.hasPermission("imonies.empty"))
			s.sendMessage(msg.getHelpMessage("MoneyEmpty"));
		if(s.hasPermission("imonies.remove"))
			s.sendMessage(msg.getHelpMessage("MoneyRemove"));
		if(s.hasPermission("essentials.balancetop") && Bukkit.getPluginManager().getPlugin("Essentials") != null)
			s.sendMessage(msg.getHelpMessage("BalTop"));
		if(s.hasPermission("imonies.version"))
			s.sendMessage(msg.getHelpMessage("MoneyVersion"));
		if(s.hasPermission("imonies.convert"))
			s.sendMessage(msg.getHelpMessage("MoneyConvert"));
		if(s.hasPermission("imonies.give"))
			s.sendMessage(msg.getHelpMessage("MoneyGive"));
		if(s.hasPermission("imonies.take"))
			s.sendMessage(msg.getHelpMessage("MoneyTake"));
		if(s.hasPermission("imonies.loan") && Utils.useLoans)
			s.sendMessage(msg.getHelpMessage("MoneyLoan"));
		if(s.hasPermission("imonies.reload"))
			s.sendMessage(msg.getHelpMessage("MoneyReload"));
		if(s.hasPermission("imonies.setowes"))
			s.sendMessage(msg.getHelpMessage("MoneySetOwes"));
		if(s.hasPermission("imonies.payall"))
			s.sendMessage(msg.getHelpMessage("MoneyPayAll"));
		if(s.hasPermission("imonies.purge"))
			s.sendMessage(msg.getHelpMessage("MoneyPurge"));
		if(s.hasPermission("imonies.transfer"))
			s.sendMessage(msg.getHelpMessage("MoneyTransfer"));
		if(s.hasPermission("imonies.bank") && Utils.useBanks)
			s.sendMessage(msg.getHelpMessage("MoneyBank"));
	}
}