package com.mciseries.iMonies.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class PayAll {
	private Messages messages = new Messages();
	
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public PayAll(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s.getName(), "payall")) {
			if(Utils.rightArgs(s.getName(), a, 2)) {
				if(Utils.isNumeric(s.getName(), a[1])) {
					for(Player pl : Bukkit.getOnlinePlayers()) {
						APi.pay(pl.getName(), Double.parseDouble(a[1]));
					}
					s.sendMessage(messages.getMessage("PayAll", "", "", Utils.formatMoney(a[1])));
				}
			}
		}
	}
}
