package com.mciseries.iMonies.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Transfer {
	private Messages messages = new Messages();
	
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Transfer(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s.getName(), "transfer")) {
			if(Utils.isNumeric(s.getName(), a[3])) {
				if(Utils.hasAccount(s.getName(), a[1])) {
					if(Utils.hasAccount(s.getName(), a[2])) {
						APi.pay(a[2], Double.parseDouble(a[3]));
						APi.charge(a[1], Double.parseDouble(a[3]));
						s.sendMessage(messages.getMessage("Transfer", a[2], "", Utils.formatMoney(a[3])));
					}
				}
			}
		}
	}
}
