package com.mciseries.iMonies.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class PayOffLoan {
	Messages messages = new Messages();
	
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public PayOffLoan(CommandSender s, Command c, String[] a) {
		if(Utils.useLoans) {
			if(Utils.hasPerms(s.getName(), "loan")) {
				if(Utils.isNumeric(s.getName(), a[1])) {
					if(Double.parseDouble(a[1]) <= APi.getOwed(s.getName()) && Double.parseDouble(a[1]) >= 0.01) {
						APi.removeOwed(s.getName(), Double.parseDouble(a[1]), true);
						s.sendMessage(messages.getMessage("PayOffLoan", "", Utils.formatMoney(a[1]), ""));
					}
					else {
						s.sendMessage(messages.getErrorMessage("OverMaxLoanAmount"));
					}
				}
			}
		}
	}
}
