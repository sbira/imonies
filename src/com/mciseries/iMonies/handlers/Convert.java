package com.mciseries.iMonies.handlers;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.earth2me.essentials.api.Economy;
import com.earth2me.essentials.api.UserDoesNotExistException;

import com.iCo6.system.Account;
import com.iCo6.system.Accounts;

import cosine.boseconomy.BOSEconomy;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Convert implements Runnable {
	private CommandSender s;
	private String[] a;
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Convert(CommandSender s, Command c, String[] a) {
		this.s = s;
		this.a = a;
	}

	@Override
	public void run() {
		if(Utils.hasPerms(s.getName(), "convert")) {
			if(Utils.rightArgs(s.getName(), a, 2)) {
				OfflinePlayer[] all = Bukkit.getOfflinePlayers();
				if(a[1].equalsIgnoreCase("essentials")) {
					s.sendMessage("Moving accounts to iMonies... you may get a 'read timed out' but leave the server running.");
					for(OfflinePlayer p : all) {
						if(Economy.playerExists(p.getName())) {
							try {
								APi.setBalance(p.getName(), Economy.getMoney(p.getName()));
							} catch (UserDoesNotExistException e) {
								
							}
						}
					}
					s.sendMessage("Done!");
				}
				else if(a[1].equalsIgnoreCase("boseconomy")) {
					s.sendMessage("Moving accounts to iMonies... you may get a 'read timed out' but leave the server running.");
					for(OfflinePlayer p : all) {
						BOSEconomy bose = (BOSEconomy) Bukkit.getPluginManager().getPlugin("BOSEconomy");
						if(bose.playerRegistered(p.getName(), false)) {
							APi.setBalance(p.getName(), bose.getPlayerMoneyDouble(p.getName()));
						}
					}
					s.sendMessage("Done!");
				}
				else if(a[1].equalsIgnoreCase("iconomy")) {
					s.sendMessage("Moving accounts to iMonies... you may get a 'read timed out' but leave the server running.");
					for(OfflinePlayer p : all) {
						Accounts icos = new Accounts();
						if(icos.exists(p.getName())) {
							Account ico = new Account(p.getName());
							APi.setBalance(p.getName(), ico.getHoldings().getBalance());
						}
					}
					s.sendMessage("Done!");
				}
				else {
					s.sendMessage("Incorrect plugin!");
				}
			}
		}
	}
}
