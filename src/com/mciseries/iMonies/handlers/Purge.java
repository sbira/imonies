package com.mciseries.iMonies.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Purge {
	Messages messages = new Messages();
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Purge(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s.getName(), "purge")) {
			if(Utils.rightArgs(s.getName(), a, 1)) {
				s.sendMessage("Not implemented.");
			}
		}
	}
}
