package com.mciseries.iMonies.handlers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.system.Utils;

public class Update {
	public Update(CommandSender s, Command c, String[] a) {
		if(!Utils.warnedUpdaters.contains(s.getName())) {
			s.sendMessage("Warning! Updating is a very experimental feature, and may damage your server if used incorrectly.");
			s.sendMessage("To be sure not to break it, make sure not to enter a version not on BukkitDev.");
			s.sendMessage("Use the format '/money update 3.1' with 3.1 being the version to update to. Enter the command again to update.");
			Utils.warnedUpdaters.add(s.getName());
		}
		else {
			Utils.warnedUpdaters.remove(s.getName());
			if(Utils.rightArgs(s.getName(), a, 2)) {
				if(Utils.isNumeric(s.getName(), a[1])) {
					if(Double.parseDouble(a[1]) < 2.2) {
						try {
							s.sendMessage("Downloading v" + a[1] + " to plugins/iMonies.jar...");
							URL newJar = new URL("http://api.bukget.org/3/plugins/bukkit/imonies/" + a[1] + "/download");
							ReadableByteChannel rbc = Channels.newChannel(newJar.openStream());
							File jar = new File("plugins/iMonies.jar");
							jar.delete();
							jar.createNewFile();
							jar = new File("plugins/iMonies.jar");
							FileOutputStream fos = new FileOutputStream(jar);
							fos.getChannel().transferFrom(rbc, 0, 1 << 24);
							fos.flush();
							fos.close();
							rbc.close();
							s.sendMessage("Downloaded iMonies. Please reload/restart server.");
						} catch (MalformedURLException e) {
							s.sendMessage("Error downloading... check the console for info.");
							e.printStackTrace();
						} catch (IOException e) {
							s.sendMessage("Error downloading... check the console for info.");
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
}
