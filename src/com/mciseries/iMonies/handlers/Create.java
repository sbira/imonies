package com.mciseries.iMonies.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Create {
	Messages messages = new Messages();
	
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Create(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s.getName(), "create")) {
			if(Utils.rightArgs(s.getName(), a, 2)) {
				APi.createAccount(a[1]);
				s.sendMessage(messages.getMessage("Create", a[1], "", ""));
			}
		}
	}
}