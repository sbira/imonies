package com.mciseries.iMonies.handlers;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Top {
	private OfflinePlayer[] pl = Bukkit.getOfflinePlayers();
	private String[] plA = {};
	private Double[] balA = {};
	private ArrayList<String> plAL = new ArrayList<String>(Arrays.asList(plA));
	private ArrayList<Double> balAL = new ArrayList<Double>(Arrays.asList(balA));
	
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Top(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s.getName(), "top")) {
			for(OfflinePlayer p : pl) {
				if(hasAcct(p.getName())) {
					plAL.add(p.getName());
					balAL.add(APi.getBalance(p.getName()));
				}
			}
			String[] names = plAL.toArray(new String[0]);
			Double[] bals = balAL.toArray(new Double[0]);
			int pages = names.length / Utils.topAccountsPerPage;
			if(pages == 0)
				pages = 1;
			if(a.length < 2) {
				s.sendMessage("Page 1/" + pages + " with " + names.length + " accounts.");
				for(int i = 0; i < Utils.topAccountsPerPage; i++) {
					if(names.length > i)
						s.sendMessage(names[i] + ":" + Utils.formatMoney(bals[i]));
				}
			}
			else {
				int start = Utils.topAccountsPerPage * Integer.parseInt(a[1]) - (Utils.topAccountsPerPage);
				int end = Utils.topAccountsPerPage * Integer.parseInt(a[1]);
				s.sendMessage("Page 1/" + pages + " with " + names.length + " accounts.");
				for(int i = start; i < end + 1; i++) {
					if(names.length > i)
						s.sendMessage(names[i] + ":" + Utils.formatMoney(bals[i]));
				}
			}
		}
	}
	/**
	 * @param acctName Account name to check if exists
	 * @return Whether the account exists or not
	 */
	public boolean hasAcct(String acctName) {
		if(APi.hasAccount(acctName))
			return true;
		else
			return false;
	}
	
	/**
	 * @param array Array of double values
	 * @param ascending Whether to sort it acending or not
	 */
	public void sort(Double[] array, boolean ascending) {
		for (int i = 0; i < array.length - 1; i++) {
			for (int j = i; j < array.length; j++) {
				if (ascending) {
					if (array[i] > array[j]) {
						double temp = array[i];
						array[i] = array[j];
						array[j] = temp;
					}
				} else{
					if (array[i] < array[j]) {
						double temp = array[i];
						array[i] = array[j];
						array[j] = temp;
					}
				}
			}
		}
	}
}
