package com.mciseries.iMonies.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Remove {
	private Messages messages = new Messages();
	
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Remove(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s.getName(), "remove")) {
			if(Utils.rightArgs(s.getName(), a, 2)) {
				APi.setBalance(a[1], 0.00);
				APi.setBankBalance(a[1], 0.00);
				APi.setOwed(a[1], 0.00);
				s.sendMessage(messages.getMessage("Remove", a[1], "", ""));
			}
		}
	}
}