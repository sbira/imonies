package com.mciseries.iMonies.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.iMonies.api.APi;
import com.mciseries.iMonies.system.Messages;
import com.mciseries.iMonies.system.Utils;

/**
 * @author rtainc
 *
 */
public class Loan {
	private Messages messages = new Messages();
	
	/**
	 * @param s
	 * @param c
	 * @param a
	 */
	public Loan(CommandSender s, Command c, String[] a) {
		if(Utils.useLoans) {
			if(Utils.hasPerms(s.getName(), "loan")) {
				if(Utils.rightArgs(s.getName(), a, 2)) {
					if(Utils.isNumeric(s.getName(), a[1])) {
						if(Double.parseDouble(a[1]) + APi.getOwed(s.getName()) <= Utils.maxOwed) {
							APi.addOwed(s.getName(), Double.parseDouble(a[1]), true);
							s.sendMessage(messages.getMessage("Loan", "", Utils.formatMoney(a[1]), ""));
						}
						else {
							s.sendMessage(messages.getErrorMessage("OverMaxLoanAmount"));
						}
					}
				}
			}
		}
		else {
			s.sendMessage(messages.getErrorMessage("LoansNotEnabled"));
		}
	}
}
